function pesinit()
use standalone
implicit real*8(a-h,o-z)
call pes_init()
a=0.0d0
end function

double precision function vint(zm,nat,nb,nc,nd)
use standalone
implicit real*8(a-h,o-z)
real*8,dimension(3,4)::xyz
real*8,dimension(3,4)::zm
integer,dimension(4)::NAT
integer,dimension(4)::NB,NC,ND

call dtoint(zm,nat,nb,nc,nd,xyz)
vint=f(xyz)
end function

double precision function vpot(x)
use standalone
implicit real*8(a-h,o-z)
real*8,dimension(3,4)::xyz
real*8,dimension(12)::x

xyz(1,1)=x(1)
xyz(2,1)=x(2)
xyz(3,1)=x(3)

xyz(1,2)=x(4)
xyz(2,2)=x(5)
xyz(3,2)=x(6)

xyz(1,3)=x(7)
xyz(2,3)=x(8)
xyz(3,3)=x(9)

xyz(1,4)=x(10)
xyz(2,4)=x(11)
xyz(3,4)=x(12)

vpot=f(xyz)
end function

subroutine dtoint(zm,nat,nb,nc,nd,xyz)
implicit real*8(a-h,o-z)
real*8,dimension(3,4)::xyz
real*8,dimension(3,4)::ZM,coord
integer,dimension(4)::NAT
integer,dimension(4)::NB,NC,ND
PI = 4.0d0*DATAN(1.d0)
NATOMS=4
!NAT(1)=53
!NAT(2)=1
!NAT(3)=6
!NAT(4)=1
!!!
!ZM(1,1)=0.0d0  !KOTESHOSSZ
!ZM(2,1)=0.0d0  !KOTESSZOG
!ZM(3,1)=0.0d0  !DIEDERES SZOG
!NB(1)=0  !ETTOL AZ ATOMTOL VAN KOTESHOSSZNYIRA
!NC(1)=0  !EZZEL ZAR BE SZOGET
!ND(1)=0  !EZZEL ZAR BE DIEDERES SZOGET
!!!
!ZM(1,2)=1.6083300d0
!ZM(2,2)=0.0D0
!ZM(3,2)=0.0d0
!NB(2)=1
!NC(2)=0
!ND(2)=0
!!!
!ZM(1,3)=2.50d0
!zm(2,3)=178.0d0
!ZM(3,3)=0.0d0
!NB(3)=2
!NC(3)=1
!ND(3)=0
!!
!ZM(1,4)=0.967603d0
!ZM(2,4)=135.347137d0
!ZM(3,4)=-0.42247d0
!NB(4)=3
!NC(4)=2
!ND(4)=1

DO I=1,NATOMS
  ZM(2,I)=ZM(2,I)*PI/180.0D0
  ZM(3,I)=ZM(3,I)*PI/180.0D0
END DO

CALL Z2C(NATOMS,ZM,COORD,NB,NC,ND)
!
DO I=1,NATOMS
  ZM(2,I)=ZM(2,I)/PI*180.0D0
  ZM(3,I)=ZM(3,I)/PI*180.0D0
END DO
!
do j=1,3
  xyz(j,1)=coord(j,2) !egy hidrogen
  xyz(j,2)=coord(j,4) !masik hidrogen
  xyz(j,3)=coord(j,3) !oxigen
  xyz(j,4)=coord(j,1) !jod
enddo

end subroutine


!Zmatrixbol Descartes koordinatakat eloallito
!program. Forras: Tasi Gyula Szamitogepes
!Kemia, JATEPress Szeged 2010, pp.23

SUBROUTINE Z2C(NATOMS,ZM,COORD,NB,NC,ND)
IMPLICIT REAL*8(A-H,O-Z)
DIMENSION ZM(3,4),COORD(3,4),NB(4),NC(4),ND(4)
DIMENSION RCB(3),RDC(3),SL(3),SN(3),SM(3)

DO I=1,NATOMS
  COORD(1,I)=0.0D0
  COORD(2,I)=0.0D0
  COORD(3,I)=0.0D0
END DO
IF(NATOMS.EQ.1) RETURN
     
COORD(1,2)=ZM(1,2)  
IF(NATOMS.EQ.2) RETURN
      
IF(NB(3).EQ.2) THEN
  COORD(1,3)=COORD(1,2)-ZM(1,3)*COS(ZM(2,3))
  COORD(2,3)=ZM(1,3)*SIN(ZM(2,3))
ELSEIF(NB(3).EQ.1) THEN
  COORD(1,3)=ZM(1,3)*COS(ZM(2,3))
  COORD(2,3)=ZM(1,3)*SIN(ZM(2,3))
ENDIF
IF(NATOMS.EQ.3) RETURN
    
DO J=4,NATOMS
        
  DO I=1,3
    RCB(I)=COORD(I,NB(J))-COORD(I,NC(J))
    RDC(I)=COORD(I,NC(J))-COORD(I,ND(J))
  ENDDO
        
  DO I=1,3
    SL(I)=RCB(I)/NORM2(RCB)
  ENDDO
        
  CALL CROSSP(RDC,RCB,SN)
        
  DO I=1,3
    SN(I)=SN(I)/NORM2(SN)
  ENDDO
        
  CALL CROSSP(SN,SL,SM)
        
  SLA=-ZM(1,J)*COS(ZM(2,J))
  SMA=ZM(1,J)*SIN(ZM(2,J))*COS(ZM(3,J))
  SNA=ZM(1,J)*SIN(ZM(2,J))*SIN(ZM(3,J))
        
  DO I=1,3
    COORD(I,J)=COORD(I,NB(J))+SLA*SL(I)+SMA*SM(I)+SNA*SN(I)
  ENDDO
ENDDO
            
RETURN
END SUBROUTINE

SUBROUTINE CROSSP(X,Y,Z)
IMPLICIT REAL*8(A-H,O-Z)
DIMENSION X(3),Y(3),Z(3)

Z(1)=X(2)*Y(3)-X(3)*Y(2)
Z(2)=-X(1)*Y(3)+X(3)*Y(1)
Z(3)=X(1)*Y(2)-X(2)*Y(1)

RETURN      
END SUBROUTINE
     

