## The MSA program

The MSA (Monomial Symmetrization Approach) is an efficient way to represent potential energy surfaces of molecular systems in cases when permutational invariance of the atoms is crucial. 

An easy-to-use MSA code has been developed in Prof. Bowman's lab by Zhen Xie and it is accessible [at GitHub here](https://github.com/Kee-Wang/PES-Fitting-MSA). An extension of the method is also being used which can be reached [here](https://github.com/szquchen/MSA-2.0).

To find all relevant imformation regarding the theoretical background of the MSA code or how to install and use it, please consult one of the GitHub repos mentioned above. This _README_ is not concerned with setting up and running MSA properly and it is assumed that you have created the files _basis.f90_, _gradient.f90_, _pes_shell.f90_, _coeff.dat_. The compiled modules are not needed.

## The scope of this repo

By using the native MSA code to fit potential energy surfaces one ends up with multiple files that constitute the whole PES. It is not extraordinarily difficult to use them in FORTRAN codes since the files produced are written in FORTRAN themselves, but sometimes it is advantageous to use Python scripts too.

By using the code snippets in this repository it is relatively straightforward to use the PES in any kind of Python code. 

## Requied packages

To use the code snippets provided, a running version of Python is requied. I suggest using Python3, however I am aware that MSA is written in Python2. In order to run the MSA codes a working set-up of FORTRAN compiling environment is also needed. 

Apart from a working Python and FORTRAN environment the only thing needed is the _f2py_ module. The suggested way to get _f2py_ is to install _NumPy_ which contains it natively. To install _NumPy_ with _pip_ simply use the following command:

    pip3 install numpy

If you don't already have _pip3_ installed you can simply type the following command on Ubuntu or Debian Linux distros:

    sudo apt-get install python3-pip 

## Running the codes

First make sure that you have files  _basis.f90_, _gradient.f90_, _pes_shell.f90_, _coeff.dat_ and _callpes.f90_ in your current working directory. First run the pes_cleanup.py script to create a single FORTRAN file for your PES (warning: the script deletes the file _coeff_fortran.dat_ from the directory). 

    python3 pes_cleanup.py
    
After running the command an choosing a filename when prompted, you may now use a single file for all intents and purposes and you do not need to carry all four MSA outputs around. 

To compile the PES file with your _callpes.f90_ FORTRAN file to python, simply type:

    f2py -c callpes.f90 pes_standalone.f90 -m PES
    
Note that _callpes.f90_, as it is in this commit, is hard-coded for a four-atomic system. It has to be changed a bit to commodate other systems.
    
## Import the modules to Python

An example for how to use the PES in a Python code is given in _scimin.py_.

The most important thing to note here is that when the surface is compiled with the supplied _callpes.f90_, then the functions _vpot_ and _pesinit_ should be imported to python, then they can simply be called as they were python functions.

The example given this file uses a _scipy_ module to optimize geometries of the PES.

## Compile to RPMD calculation

Another example of python integration is the RPMDRate code from Yury Suleimanov. An user's guide can be found [here](https://greengroup.mit.edu/rpmd-user-guide-toc) where one can find all the details for setting up the calculation.

The two files included in this repo are _4atompes.f90_ and _PES.pyf_. The former contains the link between the surface and RPMDRate, while the latter is the auxiliary file prepared as shown in the manual. With these files, the RPMD run can be compiled as:

    f2py -c pes_standalone.f90 hi_oh_pes.f90 PES.pyf
    
This creates the _PES.so_ file in the directory, which is accessible to a proper RPMDRate input.