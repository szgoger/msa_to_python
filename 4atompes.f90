      subroutine initialize_potential()
         call pes_init()
      end subroutine initialize_potential

      subroutine get_potential(q,Natoms,Nbeads,V,dVdq,info)
      implicit real*8(a-h,o-z)
      integer, intent(in) :: Natoms
      integer, intent(in) :: Nbeads
      double precision, intent(in) :: q(3,Natoms,Nbeads)
      double precision, intent(out) :: V(Nbeads)
      double precision, intent(out) :: dVdq(3,Natoms,Nbeads)
      dimension xyz(3,4)    

      integer k, info
        info = 0 
      do k = 1, Nbeads
      
        xyz(1,1)=q(1,1,k)
        xyz(2,1)=q(2,1,k)
        xyz(3,1)=q(3,1,k)

        xyz(1,2)=q(1,2,k)
        xyz(2,2)=q(2,2,k)
        xyz(3,2)=q(3,2,k)

        xyz(1,3)=q(1,3,k)
        xyz(2,3)=q(2,3,k)
        xyz(3,3)=q(3,3,k)

        xyz(1,4)=q(1,4,k)
        xyz(2,4)=q(2,4,k)
        xyz(3,4)=q(3,4,k)
        
        V(k) = f(xyz)

        step=1.0D-4
        nn=1
        do j=1,Natoms
          do i=1,3
            xyz(i,j)=xyz(i,j)-step
            dVtemp1 = f(xyz)
            xyz(i,j) = xyz(i,j) + 2.d0*step
            dVtemp2 = f(xyz)
            dVdq(i,j,k)=(dVtemp2-dVtemp1)/(2.d0*step)
            xyz(i,j)=xyz(i,j)-step
          enddo
        enddo

      end do
    
      end subroutine

