from PES import vpot, pesinit  #This is the important link between the PES and this code
from scipy.optimize import minimize
import numpy as np

def write_geom():
    final = open("optimized.xyz",  "w")
    final.write("4 \n")
    final.write("Optimized in Descartes coordinates " + str(vpot(res.x)) + "\n")
    res.x=res.x*c
    final.write("H "+ str(res.x[0])+ " " + str(res.x[1])+ " " + str(res.x[2]) + "\n")
    final.write("H " + str(res.x[3])+ " " + str(res.x[4])+ " " + str(res.x[5]) + "\n")
    final.write("O " + str(res.x[6])+ " " + str(res.x[7])+ " " + str(res.x[8]) + "\n")
    final.write("I " + str(res.x[9])+ " " + str(res.x[10])+ " " + str(res.x[11]) + "\n")
    final.close()

pesinit()

x = np.zeros(12)

##A guess for a geometry of a reactant for the HI+OH surface
#H
x[0]=101.61105360742395
x[1]=5.4775559121227967E-002
x[2]=5.3747623950862432E-002

#H
x[3]=-100.97349053424021
x[4]=-1.1789218552728433E-002
x[5]=1.4173449352416830E-002
#
#O
x[6]=-100.00389959278009
x[7]=-9.6527828700015399E-003
x[8]=2.8020987362575326E-003

#I
x[9]=99.992210372371702
x[10]=5.4342843622469654E-002
x[11]=5.6915748963222622E-002


c=0.529177 #Conversion to bohr
x=x/c

##Optimizing the potential, for finding minima
res = minimize(vpot,x,method='Nelder-Mead',tol=1e-5)
print(vpot(res.x))
write_geom()
