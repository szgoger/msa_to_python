import os

try:
    os.remove("coeff_fortran.dat")
    os.remove("pes_standalone.f90")
except OSError:
    pass

counter=0

f = open("coeff.dat", "r")
fout = open("coeff_fortran.dat", "w")

for line in f:
    counter+=1
    fout.write('coeff(' + str(counter) + ')=' + line)

f.close()
fout.close()

allfile = []
data = []
filenames = ['basis.f90', 'gradient.f90', 'coeff_fortran.dat','pes_shell.f90']
with open('pes_standalone.f90', 'w') as outfile:
    for fname in filenames:
        with open(fname) as infile:
            data.extend(infile.read().split('\n'))
        if fname == 'basis.f90':
            del data[-1]
        elif fname == 'gradient.f90':
            del data[0]
            del data[0]
            del data[0]
            del data[0]
            del data[-1]
        elif fname == 'pes_shell.f90':
            for i in range(0,27):
                del data[0]
            del data[-1]
        allfile.extend(data)
        data = []
    for item in allfile:
        outfile.write("%s\n" % item)

f = open("pes_standalone.f90", "rt")
fout = open("pes_standalone_tmp.f90", "wt")

for line in f:
    if 'real,' in line:
        writeme = line.replace('real,','real*8,')
    elif 'end module' in line:
        writeme = '\n'
    elif 'module basis' in line:
        writeme = line.replace('basis','standalone')
    elif 'module' in line:
        writeme = '\n'
    elif 'use' in line:
        writeme = '\n'
    elif 'end subroutine devpoly' in line:
        writeme = line + 'subroutine pes_init() \n' + 'real*8,dimension(1:' + str(counter) + ')::coeff \n' + 'common/pes/coeff'
    elif ('coeff(' + str(counter)) in line:
        writeme = line + 'end subroutine pes_init \n'
    elif ('function f' in line or 'function g' in line) and 'end' not in line:
        writeme = line + 'real*8,dimension(1:' + str(counter) + ')::coeff \n' + 'common/pes/coeff \n'
    else:
        writeme = line
    fout.write(writeme)

fout.write('end module standalone')

f.close()
fout.close()
os.rename('pes_standalone_tmp.f90', 'pes_standalone.f90')
